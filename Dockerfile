FROM caddy:latest
EXPOSE 80
COPY Caddyfile /etc/caddy/Caddyfile
COPY index.html /usr/src/pages/index.html
ENTRYPOINT caddy run
